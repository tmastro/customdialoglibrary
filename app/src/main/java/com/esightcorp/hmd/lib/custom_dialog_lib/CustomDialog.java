package com.esightcorp.hmd.lib.custom_dialog_lib;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;


public class CustomDialog extends BaseObservable {
    private String TAG = "TOM";
    private Context mContext;
    private String dialogButtonPos;
    private String dialogButtonNeg;
    private String description;
    private String dialogTitle;
    private int cancellable ;

    public CustomDialog(Context context){
        mContext = context;
    }

    // Display Dialog No Cancel Button
    public void displayDialog(String title, String description, String positiveText ,
                              String color, String textSize,
                              CustomDialogView.ButtonClickInterface buttonInterface) {
        setTheme(color);
        setCancellable(View.GONE);
        setDialogButtonPos(positiveText);
        setDescription(description);
        setDialogTitle(title);


/*        try {
            Class cls = Class.forName("com.esightcorp.hmd.lib.hmd_confiuration_lib.ConfigurationLibrary");

            //this will get the public constructors for the class
            Constructor[] publicConstructors = cls.getConstructors();

            for (int i = 0; i < publicConstructors.length; i++){
                Log.i(TAG, "Constructor " + publicConstructors[i].toString());
            }

            //this will return only the public methods
            Method[] methods = cls.getMethods();

            for (int i = 0; i < methods.length; i++){
                Log.i(TAG, "Method " + i + " " + methods[i].toString());

                if ( methods[i].equals("public void com.esightcorp.hmd.lib.hmd_confiuration_lib.ConfigurationLibrary.requestCurrentSharedPreference(int,java.util.Set)")){

                }
            }


            Set<String> keySet = new HashSet<>();
            keySet.add("STYLE_SIZE");
            keySet.add("STYLE");
            keySet.add("LANGUAGE??");
            configurationLibrary.requestCurrentSharedPreference(ConfigurationServiceID, keySet);
            configurationLibrary = new ConfigurationLibrary(context.getContentResolver(), new Callback() {
            .
            .
            .
             @Override
            public void onCurrentQueryResultReady(int callerID, ConfigurationLibrary.ConfigStatus configStatus, String message, List<ConfigData> configData) {
                for (ConfigData con : configData) {
                if (con.getmKey().equalsIgnoreCase("STYLE")){
                        String style = con.getmCurrentValue();
                        mOverlayInterface.updateTheme(style);
                    }
                    }


        } catch (ClassNotFoundException e){
            e.printStackTrace();
        }*/

        CustomDialogView dialog = new CustomDialogView(mContext, this, buttonInterface, textSize);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_DIALOG);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        dialog.show();

    }

    // Display Dialog Cancel Button
    public void displayDialog(String title, String description, String positiveText, String negativeText,
                              String color, String textSize,
                              CustomDialogView.ButtonClickInterface buttonInterface) {
        setTheme(color);
        setCancellable(View.VISIBLE);
        setDialogButtonPos(positiveText);
        setDialogButtonNeg(negativeText);
        setDescription(description);
        setDialogTitle(title);

        CustomDialogView dialog = new CustomDialogView(mContext, this, buttonInterface, textSize);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_DIALOG);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        dialog.show();
    }


    @Bindable
    public String getDialogTitle(){
        return dialogTitle;
    }

    public void setDialogTitle(String s) {
        dialogTitle = s;
        notifyPropertyChanged(BR.dialogTitle);
    }

    @Bindable
    public Integer getCancellable() {return cancellable;}

    public void setCancellable(Integer i) {
        cancellable = i;
        notifyPropertyChanged(BR.cancellable);
    }

    @Bindable
    public String getDialogButtonPos() {return dialogButtonPos;}

    public void setDialogButtonPos(String s) {
        dialogButtonPos = s;
        notifyPropertyChanged(BR.dialogButtonPos);
    }

    @Bindable
    public String getDialogButtonNeg() {return dialogButtonNeg;}

    public void setDialogButtonNeg(String s) {
        dialogButtonNeg = s;
        notifyPropertyChanged(BR.dialogButtonNeg);
    }

    @Bindable
    public String getDescription() {return description;}

    public void setDescription(String s) {
        description = s;
        notifyPropertyChanged(BR.description);
    }

    public void setTheme(String s){
        switch(s) {
            case "0":
                mContext.setTheme(R.style.ThemeBlackWhite);

                break;
            case "1":
                mContext.setTheme(R.style.ThemeWhiteBlack);
                break;
            case "2":
                mContext.setTheme(R.style.ThemeBlueYellow);
                break;
            case "3":
                mContext.setTheme(R.style.ThemeYellowBlue);
                break;
            default:
                break;
        }
    }



}
